## 调试部署+V: json59420
### 承接各类软件系统开发、毕设。（java、php、python自动化等等）
![输入图片说明](%E4%B8%AA%E4%BA%BA%E5%BE%AE%E4%BF%A1.png)
## 介绍
PHP 开发的图书管理系统.

### 一、Windows 部署（windows + phpstudy8 安装）
~~~
1、phpstudy下载地址：https://www.xp.cn/download.html。
2、环境：Nginx/Apache + Mysql + php7.3.4
3、将此目录下的所有文件拷贝到 phpstudy 安装目录下的 WWW 目录下。
4、在 commond.php 中配置 mysql 的连接地址。
~~~
###


### 二、运行环境

```
Nignx/Apache
PHP 7.1 ~ 7.4 
MySQL 5.7
```

### 三、🖼️ 截图预览
|系统图片      |    项目结构|
| :--------: | :--------:|
| ![图书管理系统](%E5%9B%BE%E4%B9%A6%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F.png "图书管理系统")|![项目结构](%E9%A1%B9%E7%9B%AE%E7%BB%93%E6%9E%84.png "项目结构")|
