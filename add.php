<?php
if (!defined('BOOK_WEB')){
   header('location:index.php');
}
include_once 'common.php';
$name=$_POST['book_name'];
$category=$_POST['category'];
$price=$_POST['price'];
$publish_time=$_POST['publish_time'];
//echo $name,$category,$price,$publish_time;
$sql='insert into books (name,category,price,publish_time)values (?,?,?,?)';
$sth=$pdo->prepare($sql);
$sth->bindParam(1,$name,PDO::PARAM_STR);
$sth->bindParam(2,$category,PDO::PARAM_STR);
$sth->bindParam(3,$price,PDO::PARAM_STR);
$sth->bindParam(4,$publish_time,PDO::PARAM_STR);
$sth->execute();
if ($sth->errorCode()==0) {
    echo '<script>alert("添加成功");window.location.href="index.php";</script>';
} else {
    echo '<script>alert("添加失败");window.location.href="index.php";</script>';
}