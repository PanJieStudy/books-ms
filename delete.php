<?php
if (!defined('BOOK_WEB')){
    header('location:index.php');
}
require_once 'common.php';
$id=$_GET['id'];
$sql="DELETE FROM books WHERE id=?";
$sth=$pdo->prepare($sql);
$sth->bindParam(1,$id,PDO::PARAM_INT);
if ($sth->execute()){
    echo '<script>alert("删除成功");window.location.href="index.php";</script>';
}else{
    echo '<script>alert("删除失败");window.location.href="index.php";</script>';
}