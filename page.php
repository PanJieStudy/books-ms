<?php
function make_page($page,$pagesize){
    $start=($page-1)*$pagesize;
    return " limit $start,$pagesize";
}
function make_page_html($page,$max_page){
    $next_page=$page+1;
    if($next_page>$max_page){
        $next_page=$max_page;
    }
    $pre_page=$page-1;
    if($pre_page<1){
        $pre_page=1;
    }
    $pagehtml='<a href="index.php?page=1">首页</a>';
    $pagehtml.='<a href="index.php?page='.$pre_page.'">上一页</a>';
    $pagehtml.='<a href="index.php?page='.$next_page.'">下一页</a>';
    $pagehtml.='<a href="index.php?page='.$max_page.'">末页</a>';
    return $pagehtml;
}
function makePageHtml($page,$max_page){
    //保存GET参数数组，并删除page元素
    //计算下一页
    $next_page = $page +1;
    //判断下一页的页码是否大于最大页码值，如果大于则把最大页码值赋值给它
    if($next_page > $max_page)  $next_page =  $max_page;
    //计算上一页
    $prev_page = $page - 1;
    //判断上一页的页码是否小于1，如果小于则把1赋值给它
    if($prev_page < 1 ) $prev_page  = 1;
    //重新拼接分页链接的html代码
    $page_html = '<a href="index.php?page=1">首页</a>';
    $page_html .= "<a href='index.php?page=".$prev_page."'>上一页</a>";
    $page_html .= "<a href='index.php?page=".$next_page."'>下一页</a>";
    $page_html .= "<a href='index.php?page=".$max_page."'>末页</a>";
    //返回分页链接
    return $page_html;
}