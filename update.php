<?php
if (!defined('BOOK_WEB')){
    header('location:index.php');
}
include_once 'common.php';
$id=$_POST['id'];
$name=$_POST['book_name'];
$category=$_POST['category'];
$price=$_POST['price'];
$publish_time=$_POST['publish_time'];
$sql="UPDATE books SET name=?,category=?,price=?,publish_time=? WHERE id=?";
$sth=$pdo->prepare($sql);
$sth->bindParam(1,$name,PDO::PARAM_STR);
$sth->bindParam(2,$category,PDO::PARAM_STR);
$sth->bindParam(3,$price,PDO::PARAM_STR);
$sth->bindParam(4,$publish_time,PDO::PARAM_STR);
$sth->bindParam(5,$id,PDO::PARAM_INT);
if ($sth->execute()){
    echo '<script>alert("修改成功");window.location.href="index.php";</script>';
}else{
    echo '<script>alert("修改失败");window.location.href="index.php";</script>';
}